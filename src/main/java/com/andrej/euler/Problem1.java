package com.andrej.euler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem1 {

	private int findSum(int total) {
		int sum = 0;
		for (int i=1;i<total;i++) {
			if (i%3 == 0) {
				sum += i;
			} else
			if (i%5 == 0) {
				sum += i;
			}
		}
		return sum;
	}
	
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem1 problem = new Problem1();
		int result = problem.findSum(10);
		logger.info("Result for total of " + 10 + " is: " + result);
		result = problem.findSum(1000);
		logger.info("Result for total of " + 1000 + " is: " + result);
	}
}
