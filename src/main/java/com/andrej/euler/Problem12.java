package com.andrej.euler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem12 {

	private static int getTriangle(int order) {
		int triangle = 0;
		for (int i=1; i <= order; i++) {
			triangle += i ;
		}
		return triangle;
	}
	
	private int findTriangle(int divisors) {
		int triangle = getTriangle(1);
		if (divisors == 1)	//special case
			return triangle;

		for (int order=2; ; order++) {
			int foundDivisors = 2;
			triangle = getTriangle(order);
			for (int divisor=2; divisor <= Math.sqrt(triangle); divisor++) {
				if (triangle % divisor == 0) {
					if (divisor*divisor == triangle) {
						foundDivisors += 1;
					} else {
						foundDivisors += 2;
					}
					if (foundDivisors > divisors) {
						return triangle;
					}
				}
			}
		}
	}
	
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem12 problem = new Problem12();
		int result = getTriangle(7);
		logger.info("7th triangle is: " + result);
		
		result = problem.findTriangle(5);
		logger.info("First triangle with over 5 divisors is: " + result);
		result = problem.findTriangle(500);
		logger.info("First triangle with over 500 divisors is: " + result);
	}
}
