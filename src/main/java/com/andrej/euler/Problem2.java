package com.andrej.euler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem2 {

	private int findEvenFibSum(int total) {
		int fib1=1;
		int fib2=1;
		int sum=0;
		while (fib1+fib2 <= total) {
			int fib3 = fib1+fib2;
			if ((fib3)%2 == 0)
				sum += fib3;
			fib1 = fib2;
			fib2 = fib3;
		}
		return sum;
	}

	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem2 problem = new Problem2();
		int result = problem.findEvenFibSum(4000000);
		logger.info("Result for total of " + 4000000 + " is: " + result);
	}

}
