package com.andrej.euler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem3 {

	private long findLargestFactor(long inputNumber) {
		Logger debugLogger = LogManager.getLogger();
		long largestPart = inputNumber;
		if (largestPart == 1) {
			return 1;
		}
		while (largestPart%2 == 0) {
			largestPart = largestPart/2;
			//System.out.println(largestPart);
		}
		if (largestPart == 1) {
			return 2;
		}
		for (long i=3;i<=(long)Math.floor(Math.sqrt(largestPart));i+=2) {
			while (largestPart%i == 0) {
				largestPart = largestPart/i;
				debugLogger.debug(largestPart);
			}
			if (largestPart == 1) {
				return i;
			}
		}
		return largestPart;
	}
	
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem3 problem = new Problem3();
		long result = problem.findLargestFactor(2259801992L);
		logger.info("Largest prime factor of number " + 13195 + " is: " + result);
		result = problem.findLargestFactor(600851475143L);
		logger.info("Largest prime factor of number " + 600851475143L + " is: " + result);
	}

}
