package com.andrej.euler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem9 {

	private int findTripletProduct(int sum) {
		for (int a=1; a <= sum/3+1; a++) {
			for (int b=a+1; b<=(sum-a)/2+1; b++) {
				if ((a*a + b*b) == (sum-a-b)*(sum-a-b)) {
					return a*b*(sum-a-b);
				}
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem9 problem = new Problem9();
		int result = problem.findTripletProduct(12);
		logger.info("Triplet for sum of 12: " + result);
		result = problem.findTripletProduct(1000);
		logger.info("Triplet for sum of 1000 is: " + result);
	}

}
