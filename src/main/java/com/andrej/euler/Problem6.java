package com.andrej.euler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem6 {

	private int findSumSquareDiff(int limit) {
		int sumSquare = 0;  
		for (int number=1; number <= limit; number++) {
			sumSquare += number*number;
		}
		int squareOfSum = limit*(limit+1)/2;
		squareOfSum = squareOfSum*squareOfSum;
		int diff = squareOfSum - sumSquare;
		return diff;
	}
	
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem6 problem = new Problem6();
		int result = problem.findSumSquareDiff(10);
		logger.info("Difference between the sum of the squares of the first 10 numbers and the square of the sum is: " + result);
		result = problem.findSumSquareDiff(100);
		logger.info("Difference between the sum of the squares of the first 100 numbers and the square of the sum is: " + result);
	}

}
