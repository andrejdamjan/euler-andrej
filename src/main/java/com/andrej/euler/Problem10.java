package com.andrej.euler;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem10 {

	private long findPrimesSum(int below) {
		Logger debugLogger = LogManager.getLogger();
		long sum = 5;
		Set<Integer> primeSet = new HashSet<Integer>();
		primeSet.add(2);
		primeSet.add(3);
		// currentNumer = 6*k-1 or 6*k+1
		int currentNumber = 0;
		for (int k=1; ; k++) {
			currentNumber = 6*k-1;
			for (int i=0; i<2; i++) {
				boolean foundPrime = true;
				for (int primeNumber : primeSet) {
					if (currentNumber % primeNumber == 0) {
						foundPrime = false;
						break;
					}
				}
				if (foundPrime) {
					if (currentNumber >= below)
						return sum;
					else {
						primeSet.add(currentNumber);
						sum += currentNumber;
						debugLogger.debug("Found prime: " + currentNumber);
					}
				}
				currentNumber += 2;
			}
		}
	}

	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem10 problem = new Problem10();
		long result = problem.findPrimesSum(10);
		logger.info("Sum of all primes below 10 is: " + result);
		result = problem.findPrimesSum(2000000);
		logger.info("Sum of all primes below 2,000,000 is: " + result);
	}
}
