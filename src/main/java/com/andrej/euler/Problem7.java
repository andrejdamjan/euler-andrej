package com.andrej.euler;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem7 {

	private int findPrime(int order) {
		if (order == 1)
			return 2;
		Set<Integer> primeSet = new HashSet<Integer>();
		primeSet.add(2);
		int primeCandidate = 0;
		for (int currentNumber=3;;currentNumber+=2) {
			boolean foundPrime = true;
			for (int primeNumber : primeSet) {
				if (currentNumber % primeNumber == 0) {
					foundPrime = false;
					break;
				}
			}
			if (foundPrime) {
				if (primeSet.size() == order-1) {
					primeCandidate = currentNumber;
					break;
				}
				primeSet.add(currentNumber);
			}
		}
		return primeCandidate;
	}
	
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem7 problem = new Problem7();
		int result = problem.findPrime(6);
		logger.info("The 6th prime number is: " + result);
		result = problem.findPrime(10001);
		logger.info("The 10001st prime number is: " + result);
	}

}
