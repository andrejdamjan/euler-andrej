package com.andrej.euler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem4 {

	private int findLargestPalindrome(int min, int max) {
		int mult = 0;
		int largestPalindrome = 0;
		for (int mul1=max; mul1>=min; mul1--) {
			for (int mul2=max; mul2>=min; mul2--) {
				mult = mul1*mul2;
				if (checkPalindrome(mult)) {
					if (mult > largestPalindrome) {
						largestPalindrome = mult;
					}
				}
			}
		}
		return largestPalindrome;
	}
		
	private boolean checkPalindrome(int input) {
		StringBuilder strInput = new StringBuilder(Integer.toString(input));
		StringBuilder strReverse = new StringBuilder(strInput).reverse();
		if (strInput.toString().equals(strReverse.toString())) {
			return true;
		}
		return false;
	}
		
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem4 problem = new Problem4();
		int result = problem.findLargestPalindrome(100,999);
		logger.info("Largest palindrome made from product of numbers between 100 and 999 is: " + result);
	}
}
