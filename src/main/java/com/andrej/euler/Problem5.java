package com.andrej.euler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem5 {

	private int findDivNumber(int limit) {
		int currentNumber = limit;
		int result = 0;

		do {
			boolean foundNumber = true;
			for (int divisor = limit; divisor > 1; divisor--) {
				if (currentNumber%divisor != 0) {
					foundNumber = false;
					break;
				}
			}
			if (foundNumber) {
				result = currentNumber;
				break;
			}
			currentNumber++;
		} while (true);
		
		return result;
	}
	
	public static void main(String[] args) {
		Logger logger = LogManager.getLogger();
		Problem5 problem = new Problem5();
		int result = problem.findDivNumber(10);
		logger.info("Smallest number that is evenly divisible by all of the numbers from 1 to 10 is: " + result);
		result = problem.findDivNumber(20);
		logger.info("Smallest number that is evenly divisible by all of the numbers from 1 to 20 is: " + result);
	}

}
